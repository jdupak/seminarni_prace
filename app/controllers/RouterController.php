<?php 

	class RouterController extends BasicPageController
	{
		/**
		 * Choose appropriate controller according to url data
		 * and call action (init method)
		 * 
		 * @param  [string] $param - $_SERVER['REQUEST_URI']
		 * @return [NULL] Call controllers init method
		 */
		public function init( $param )
		{
			$this->tryDB();
			$this->user();
			$parsedURL = $this->parseURL( $param );
			$this::runInit( $this->smarty, $parsedURL );
		}

		/**
		 * Try to create permanent link to db
		 * @return [NULL]
		 */
		public function tryDB()
		{
			try {
				Db::connect( DB_HOST , DB_USER , DB_PSWD , DB_NAME );	
			} catch(Exception $e) {
				U::errMsg( 'error', DB_ERROR, $this );
				$controller = new HpController( $this->smarty );
				$controller->init([]);
			}
		}

		public function user()
		{
			if ( isset( $_POST['login'] ) ) {
				$stat = User::login( $_POST['login']['username'], $_POST['login']['password'] );
				if ( $stat ) {
					$this->msg( WELCOME . " " . User::$username, 'info' );
				} else {
					$this->msg( LOGIN_FAILURE );
				}
			} elseif ( isset( $_POST['register'] ) ) {
				User::register( $_POST['register']['username'], $_POST['register']['password'], $_POST['register']['email'] );
				$this->redirect( "/?msg=" . REGISTER_SUCCESS . "&et=info" );
			} else {
				User::init();
			}

		}

		/**
		 * Extract data from url
		 * 
		 * @param  [string] $url
		 * @return [array] Array of strings from url (by slashes)
		 */
		private function parseURL( $url )
		{
			$parsedURL = explode( "/" , trim( ltrim( parse_url( $url )["path"] , "/" ) ) );
			foreach ($parsedURL as $tile) {
				$tile = preg_replace("/\W|_/", '', $tile);
			}

			if ( empty( $parsedURL[0]) ){
				$parsedURL[0] = "hp";
			} elseif ( $parsedURL[0] == "router" OR $parsedURL[0] == "basic-page" ){
				$this->redirect();
			}
			$parsedURL['origin'] = $parsedURL[0];

			return $parsedURL;
		}

		/**
		 * Make cammel notation from dash notation
		 * 
		 * @param  [string] $classNameDashed
		 * @return [string]
		 */
		private static function classNameFormat( $classNameDashed )
		{
			return str_replace( ' ', '', ucwords( str_replace( '-', ' ', $classNameDashed ) ) );
		}

		/**
		 * Route proccess initiated by another controller
		 * @param  [object] $smarty
		 * @param  [array] 	$parsedUrl Parsed URL
		 * @return [action] Call controllers init method
		 */
		public static function runInit( $smarty, $parsedURL )
		{

			$controllerName = RouterController::classNameFormat( array_shift( $parsedURL ) );

			// Rerouting by config
			$controllerClassName = (RedirectEngine::tryReroute( $controllerName ) ??  $controllerName) .  "Controller";

			if ( file_exists( APP_DIR . "controllers" . DIRECTORY_SEPARATOR . $controllerClassName . ".php" ) ) {
				$controller = new $controllerClassName( $smarty );
			} else {
				$controller = new Error404Controller( $smarty );
			}

			$controller->init( $parsedURL );
		}


		/**
		 * Force 404 manually without changing url
		 * 
		 * @param  [object] $smarty
		 * @return [action] Run Error404Controller's default task;
		 */
		public static function get404( $smarty )
		{
			$controller = new Error404Controller( $smarty );
			$controller->init( Array() );
		}
	}

 ?>