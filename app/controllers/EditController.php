<?php 

	class EditController extends BasicPageController
	{
		public $requiredClearance = 1;

		public function init( $param )
		{
			U::auto( $this );

			if ( !isset( $param[0] ) AND !is_numeric( $param[0] ) AND $param[0] !== "new" ) {
				$this->e404();
				return;
			}

			$this->urlData = $param;

			$this->tryOptions();
			$this->tryEdit();

			$cocktail = new Cocktail;
			$ingredient = new Ingredient();
			$cocktail->load( $this->urlData[0] );

			if ( $cocktail->name == NULL AND $param[0] !== "new") {
				$this->e404();
			} else {
				$this->smartyAssignAll( $cocktail );
				$this->smarty->assign( 'categories' , $cocktail->listCategories() );
				$this->smarty->assign( 'cocktailCat' , $cocktail->listCocktailCategories( $this->urlData[0] ) );
				$this->smarty->assign( 'title' , EDIT_TITLE );
				$this->template = "edit";
				$this->display();
			}
		}

		public function tryEdit()
		{
			//refactor with array diff
			if ( !empty($_POST['f_cocktail']['name']) ) {
				$cocktail = new Cocktail;
				$data[0] = $_POST['f_cocktail'];
				$data[0]['glass'] = "";		//$_POST['f_glass']; TODO
				$data[1] = $_POST['f_recipe'] ?? [];

				$cocktail = new Cocktail;
				if ( isset( $_POST['f_ingredient'] ) ) {
					foreach ($_POST['f_ingredient'] as $key => $newIngredient) {
						$newIngredient['id2'] ?? NULL;
						$newIngredient['name'] = $_POST['f_recipe_'][$key]['name'];
						$data[1][$key]['id'] = $ingredient->insert( $newIngredient ); 
					}
				}

				if ( $this->urlData[0] == "new" ) {
					$id = $cocktail->insert( $data );
					if ( empty( $id ) ) {
						$this->msg( DB_ERROR, 'error' );
					} else {
						$this->msg( CREATE_SUCCESS, 'info' );
					}
				} else {
					$id = $this->urlData[0];
					$cocktail->update( $this->urlData[0] , $data );
					$this->msg( EDIT_SUCCESS, 'info' );
					$cocktailCat = $cocktail->listCocktailCategories( $id );
					$cat = $_POST['cat'] ?? [];
					$addCat = array_diff($cat , $cocktailCat);
					$delCat = array_diff($cocktailCat, $cat);
					$cocktail->deleteCategories( $id, $delCat );
					$cocktail->addCategories( $id, $addCat );
				}
			}
		}

		public function tryOptions()
		{
			if ( is( $this->urlData[1] , "del" ) ) {
				$this->delete();
			}
		}

		public function delete()
		{
			if ( User::checkAuthority( AUTHORITY_DELETE ) ) {
				$cocktail = new Cocktail;
				$stat = $cocktail->delete($this->urlData[0]);
				
				if ($stat > 0) {
					$type = "info";
					$message = DELETE_SUCCESS;
				} else {
					$type = "error";
					$message = DELETE_ERR . " - " . NO_RECORD;
				}

				RedirectEngine::redirect("/hp?msg=". urlencode($message) . "&et=" .$type);
			} else {
				$this->msg( ACCESS_DENIED, 'error');
			}

		}
	}


 ?>