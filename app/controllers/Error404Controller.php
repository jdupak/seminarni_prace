<?php 

	class Error404Controller extends BasicPageController
	{
		/**
		 * Create error 404 page
		 * 
		 * @param  [array] $param - Url data
		 * @return [action]
		 */
		public function init( $param )
		{
			U::auto( $this );
			
			header( "HTTP/1.0 404 Not Found" );
			$this->smarty->assign( 'title' , E404_TITLE );
			$this->template = "error";
			$this->display();
		}

	}

 ?>