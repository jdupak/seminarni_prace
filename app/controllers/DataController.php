<?php 

	/**
	 * Data generator for ajax
	 *
	 * @param [string] param	- choose dataset
	 * @return [string]  		- JSON data
	 * Dataset:
	 * 	ingredient-list.json
	 * 	ingredient-variants.json
	 * 
	 */
	class DataController extends Controller
	{
		public function init( $param )
		{
			header('Content-type: application/json');

			if ( is( $param[0] , "ingredient-list.json" ) ) {
				$ingredient = new Ingredient();
				echo( json_encode( $ingredient->loadAll() ) );
			} elseif ( is( $param[0] , "ingredient-variants.json" ) ) {
				$ingredient = new Ingredient();
				echo( json_encode( $ingredient->variantOptions() ) );
			}

		}

	}

 ?>