<?php 

	class IngredientController extends BasicPageController
	{
		public $requiredClearance = 1;
	
		public function init( $param )
		{
			U::auto( $this );
			
			if ( empty( $param[0] ) ) {
				$this->listing();

			} elseif ( !is_numeric( $param[0] ) AND $param[0] !== "new" ) {
				$this->e404();
				return;
			}

			$this->urlData = $param;
			$this->tryEdit();
			$this->tryOptions();
			$this->detail();			

			$this->smarty->assign( 'title' , EDIT_TITLE );
			$this->display();
		}

		public function tryEdit()
		{
			if ( !empty($_POST['f_ingredient']['name']) )
			{
				$data = $_POST['f_ingredient'];

				$ingredient = new Ingredient;

				if ( $this->urlData[0] == "new" ) {
					if ( isset( $data['name'] ) AND isset( $data['package'] ) AND isset( $data['mu'] ) ) {
						$id = $ingredient->insert( $data );
						if ( is_integer( $id ) ) {
							$this->msg( CREATE_SUCCESS, 'info' );
						} else {
							$this->msg( CREATE_FAILURE, 'error' );
						}
					} else {
						$this->msg( CREATE_FAILURE - NOT_ENOUGH_ARGUMENTS, 'error' );
					}
				} else {
					$ingredient->edit( $this->urlData[0] , $data );
					$this->msg( EDIT_SUCCESS, 'info' );
				}
			}
		}

		public function tryOptions()
		{
			if ( is($this->urlData[1], "del" ) ) {
				$ingredient = new Ingredient;
				$err = false;

				if ( !isset( $this->urlData[0] ) OR !is_numeric( $this->urlData[0] ) ) {
					$err = true;
					$message = DELETE_ERR . " - " . PARAM_ERR;
					$type = "error";
				} else {
		            try {
						$stat = $ingredient->delete($this->urlData[0]);

		            } catch (PDOException $e) {
		            	$message = DELETE_ERR . " - " . FOREIGN_KEY_ERR;
		            	$type = "error";
		                $msg = urlencode($message);
						RedirectEngine::redirect("/hp?msg=".$msg."&et=".$type);
		            }

					if ($stat > 0) {
						$type = "info";
						$message = DELETE_SUCCESS;
					} else {
						$type = "error";
						$message = DELETE_ERR . " - " . NO_RECORD;
					}
				}

				RedirectEngine::redirect("/hp?msg=" . urlencode($message) . "&et=" . $type);
			}
		}

		public function listing()
		{
			$ingredient = new Ingredient();
			$list = $ingredient->loadAll();
			$this->smarty->assign( 'list' , $list );
			$this->template = "listIngredients";
		}

		public function detail()
		{
			$ingredient = new Ingredient();
			if ( !empty( $this->urlData[0] ) AND $this->urlData[0] !== "new" ) {
				$ingredient->load( $this->urlData[0] );

				if ( $ingredient->name == NULL ) {
					$this->e404();
					return;
				}

				$this->smartyAssignAll( $ingredient );
				$this->template = "editIngredient";
			}
		}
	}

 ?>