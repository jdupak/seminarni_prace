<?php

	abstract class Controller
	{
		/**
		 * Minimal user authorization level to access the controller
		 * @var integer
		 */
		public $requiredClearance = 0;

		/**
		 * Abstract to controller's basic action function
		 * 
		 * @param  [non_specific] $param - Usually array with url data
		 * @return [action]
		 */
		abstract function init( $param );

		/**
		 * Redirect engine module inmplement
		 * Redirect to given url with 301 code
		 * 
		 * @param  [string] $url
		 * @return [action]
		 */
		public function redirect( $url )
		{
			RedirectEngine::redirect( $url );
		}

		/**
		 * Force 404
		 * 
		 * @return [action]
		 */
		public function e404( )
		{
			RouterController::get404( $this->smarty );
		}

		/**
		 * Check authority to access the controller
		 * 
		 * @return [action]
		 */
		public function checkAuthority( )
		{
			if( !User::checkAuthority( $this->requiredClearance ) ) {
				$this->accessDenied();
				exit;
			};

		}

		/**
		 * Handle access denied state
		 * 
		 * @return [action]
		 */
		public function accessDenied( )
		{
			echo "string";
			$this->redirect("/hp?msg=". urlencode(ACCESS_DENIED) . "&et=error");
		}
	}

 ?>