<?php 

	class PageController extends BasicPageController
	{
		/**
		 * Display static
		 * 
		 * @param  [array] $param - Url data
		 * @return [action]
		 */
		public function init( $param )
		{
			U::auto( $this );
			if ( $param['origin'] == "login" AND !User::$stat ) {
				$this->smarty->assign( 'title' , HP_TITLE );
				$this->template = "login";
			} elseif ( $param['origin'] == "logout" AND User::$stat ) {
				User::logout();
				$this->msg( LOGOUT_SUCCESS, 'info');
				$this->smarty->assign( 'title' , HP_TITLE );
				$this->template = "hp";
			} elseif ( $param['origin'] == "register" AND !User::$stat ) {
				$this->smarty->assign( 'title' , HP_TITLE );
				$this->template = "register";
			} else {
				$cocktail = new Cocktail;
				$this->smarty->assign( 'categories' , $cocktail->listCategories() );
				$this->smarty->assign( 'title' , HP_TITLE );
				$this->template = "hp";
			}

			$this->display();
		}
	}

 ?>