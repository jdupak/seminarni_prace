<?php 

	class CocktailController extends BasicPageController
	{
		/**
		 * Create HP
		 * 
		 * @param  [array] $param
		 * @return [action]
		 */
		public function init( $param )
		{
			U::auto( $this );

			if ( $param['origin'] === "listing" ) {
				$this->listing( $param );
			} elseif ( $param['origin'] === "detail" ) {
				$this->detail( $param );
			} else {
				$this->e404();
			}
			
			$this->display();
		}

		public function detail( $param )
		{
			$cocktail = new Cocktail;
			$cocktail->load( $param[0] ?? 1 );

			if ( $cocktail->name == NULL ) {
				$this->msg( NOT_FOUND, 'error' );
				$this->e404();
			} else {
				$this->smartyAssignAll( $cocktail );
				$this->smarty->assign( 'title' , DETAIL_TITLE );
				$this->template = "detail";
			}
		}

		public function listing( $param )
		{
			$cocktail = new Cocktail;
			$listingData = $cocktail->list( $param );

			$this->smarty->assign( 'listing' , $listingData );
			$this->smarty->assign( 'listingParams' , $cocktail->listingParams );
			$this->smarty->assign( 'categories' , $cocktail->listCategories() );
			$this->smarty->assign( 'title' , DETAIL_TITLE );
			$this->template = "listing";
		}

	}

 ?>