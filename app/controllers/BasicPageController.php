<?php
/**
 * Abstract controller
 * 
 */

	abstract class BasicPageController extends Controller
	{
		/**
		 * Name of smarty template to be used
		 * @var string
		 */
		public $template = "default";

		/**
		 * Constructor makes smarty object available in all controllers
		 * @param [object] $smarty  - Templating engine object
		 */
		public function __construct( $smarty ) {
			$this->smarty = $smarty;
		}

		/**
		 * Init smarty view
		 * @return [NULL]
		 */
		public function display() {
			$this->loadBasicData();
			$this->smarty->display( "layout/" . $this->template . ".tpl");
		}

		/**
		 * Assign all values to smarty variables
		 * as key => value
		 * @param  [array] $array - Associative array
		 * @return [action] Assign to smarty vars
		 */
		public function smartyAssignAll( $array )
		{
			foreach ( $array as $key => $value ) {
				$this->smarty->assign( $key , $value );
			}
		}

		/**
		 * Show message on frontend
		 * @param  [string|HTML] $message
		 * @param  [string-enum] $type 	- {info, warning, error(default)}
		 * @return [NULL]
		 */
		public function msg( $message, $type = "error" )
		{
			U::errMsg( $type, $message, $this );
		}

		public function accessDenied()
		{
			$this->smarty->assign( 'title' , LOGIN );
			$this->template = "login";
			$this->msg( ACCESS_DENIED );
			$this->display();
		}

		/**
		 * Inject basic PHP data (e.g. username, URL) to SMARTY
		 * @return [NULL]
		 */
		public function loadBasicData()
		{
			$data['user']['username'] = User::$username;
			$data['user']['stat'] = User::$stat;
			$data['_get'] = $_GET;
			$data['_post'] = $_POST;
			$this->smartyAssignAll( $data );
		}
	}

 ?>