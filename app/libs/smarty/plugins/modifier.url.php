<?php
/**
 * @author Jakub Dupak
 */
function smarty_modifier_url($string)
{
    return ( empty($_SERVER['HTTPS']) ? "http://" : "https://" ) . $_SERVER['HTTP_HOST'] . "/" . $string;
}
