<?php
/**
 * @author Jakub Dupak
 */
function smarty_modifier_url_r($string)
{
    return ( empty($_SERVER['HTTPS']) ? "http://" : "https://" ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . ( substr( $_SERVER['REQUEST_URI'] , -1) == "/" ? "" : "/") . $string;
}
