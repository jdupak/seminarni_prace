<?php 
/**
 * Utilities and othe global scope functions
 */

/**
 * Test value of variable, which does not have to be set
 * @param  [string|number|boolen]  &$val1 	variable to be tested
 * @param  [string|number|boolen]  $val2 	compared value
 * @return boolean
 */
function is( &$val1, $val2 = NULL )
{
	return isset( $val1 ) AND $val1 == $val2;
}

/**
 * Creates absolute URI
 * @param  [string]  	$string 	relative path
 * @param  [boolean] 	$rel    	is relative to current location
 * @return [string]
 */
function url( $string, $rel = false )
{
	if ( $rel ) {
		return ( empty($_SERVER['HTTPS']) ? "http://" : "https://" ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . ( substr( $_SERVER['REQUEST_URI'] , -1) == "/" ? "" : "/") . $string;
	} else {
		return ( empty($_SERVER['HTTPS']) ? "http://" : "https://" ) . $_SERVER['HTTP_HOST'] . "/" . $string;
	}
}

?>