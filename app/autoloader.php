<?php 

	/**
	 * Determines controller x module and loads its class
	 * 
	 * @param  [type] $class
	 * @return [action] Load component
	 */
	function autoloader( $class )
	{
		try {

		 	if ( preg_match('/Controller$/', $class) )

	                require( APP_DIR . "controllers" . DIRECTORY_SEPARATOR . $class .".php" );

	        else

	                require( APP_DIR . "models" . DIRECTORY_SEPARATOR . $class .".php" );

		} catch ( Exception $e ) {
			throw new Exception( AUTOLOADER_NOT_FOUND );
		}
	}

	spl_autoload_register("autoloader");

?>