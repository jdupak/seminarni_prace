<?php 

	/**
	 * Universal redirect as code 301 Permanently moved
	 */
	class RedirectEngine
	{

		/**
		 * Redirect function
		 * @param  [string] $url
		 * @return [action]
		 */
		public static function redirect( $url )
		{
			$url = ROOT_URL . $url;
			header("HTTP/1.1 301 Moved Permanently"); 
			header("Location: $url");
	        header("Connection: close");
	        exit;
		}

		/**
		 * Reroute URLs not corresponding with controller names
		 * @param  [string] $oldName
		 * @return [string|NULL] new controller name or null if not found
		 */
		public static function tryReroute( $oldName )
		{
			return REROUTINGS[$oldName] ?? NULL;
		} 
	}


 ?>