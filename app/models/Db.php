<?php

	/**
	 * Database wrapper via PDO
	 * 
	 * @author Dan Štorc, Jakub Dupák
	 * @version 2.0 [Specific query types]
	 */
	class Db
	{
	    private static $connection;
	    private static $settings = array( PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8", PDO::ATTR_EMULATE_PREPARES => false );

	    /**
	     * Conect to DB
	     * 
	     * @param  [string] $host    
	     * @param  [string] $user    
	     * @param  [string] $password
	     * @param  [string] $database          
	     */
	    static function connect( $host, $user, $password, $database )
	    {
	        if ( !isset( self::$connection ) ) {
	        	try {
		            self::$connection = new PDO( 
		                "mysql:host=$host;dbname=$database", $user, $password, self::$settings
		             );
	        	} catch( Exception $e ) {
	        		throw new Exception( DB_ERROR );
	        	}
	        }
	    }

	    /**
	     * Run sql query
	     * 
	     * @param  [string] $sql 		- SQL query
	     * @param  [array]  $parametres
	     * @param  [string/enum]  $mode - PDO fetch mode
	     *                              num 	= numeric array only
	     *                              assoc 	= associative array only
	     *                              ""		= both
	     *                              keyVal	= key<->value array (for 2 columns only)
	     * @param  [string/enum]  $fetch - PDO fetch settings
	     *                               group 	= group by first column
	     *                               unique = unique only
	     *                               "" 	= normal fetch
	     * @return [array] SQL result array, all lines          
	     */
	    static function query( $sql, $parametres = array(), $mode = "", $fetch = "" )
	    {
	        $return = self::$connection->prepare( $sql );
	        $return->execute( $parametres );

	        if ( $mode == "keyVal") {
	        	$return->setFetchMode( PDO::FETCH_KEY_PAIR );
	        } elseif ( $mode == "assoc") {
	        	$return->setFetchMode( PDO::FETCH_ASSOC );
	        } elseif ( $mode == "num") {
	        	$return->setFetchMode( PDO::FETCH_NUM );
	        }

	        if ( $fetch == "group" ) {
	        	return $return->fetchAll( PDO::FETCH_GROUP|PDO::FETCH_COLUMN );
	        } elseif ( $fetch == "single" ) {
	        	return $return->fetchAll( PDO::FETCH_COLUMN );
	        } elseif ( $fetch == "unique" ) {
	        	return $return->fetchAll( PDO::FETCH_UNIQUE );
	        } else {
	        	return $return->fetchAll();
	        }
	    }

	    /**
	     * Run sql query and return all lines ito object
	     * 
	     * @param  [string] $sql - SQL query
	     * @param  [object] $object - Object to inject data
	     * @param  [array]  $parametres
	     * @return [action] Load data to object (by class)         
	     */
	    static function queryObject( $sql, $object, $parametres = array() )
	    {
	        $return = self::$connection->prepare( $sql );
	        $return->execute( $parametres );
	        $return->setFetchMode( PDO::FETCH_INTO, $object );
	        return $return->fetch();
	    }

	    /**
	     * Run sql query and return one line
	     * 
	     * @param  [string] $sql - SQL query
	     * @param  [array]  $parametres
	     * @return [array] SQL result array, one line          
	     */
	    static function queryOne( $sql, $parametres = array() )
	    {
	        $return = self::$connection->prepare( $sql );
	        $return->execute( $parametres );
	        return $return->fetch();
	    }

	    /**
	     * Run sql query and return one line, ommited one array layers
	     * 
	     * @param  [string] $sql
	     * @param  [array]  $parametres
	     * @return [array] SQL result array, one row         
	     */
	    static function querySingle( $sql, $parametres = array() )
	    {
	        $return = self::queryOne( $sql, $parametres );
	        return $return[0];
	    }

	    /**
	     * Runs query and returns single value
	     * - use for SELECT count(x)
	     * 
	     * @param  [string] $sql
	     * @param  [array]  $parametres
	     * @return [string]	single value
	     */
	    static function count( $sql, $parametres = array() )
	    {
	        $return = self::$connection->prepare( $sql );
	        $return->execute( $parametres );
	        return $return->fetchColumn();
	    }

	    /**
	     * Runs query and returns number of affcted rows
	     * - use for INSERT and UPDATE
	     * 
	     * @param  [string] $sql
	     * @param  [array]  $parametres
	     * @return [string]	single value
	     */
	    static function edit( $sql, $parametres = array() )
	    {
	        $return = self::$connection->prepare( $sql );
	        $return->execute( $parametres );
	        return $return->rowCount();
	    }
	    
		/**
		 * Get id of last affected row
		 * 
		 * @return [int] Row id
		 */
	    public static function getLastId() {
	        return self::$connection->lastInsertId();
	    }

	    /**
	     * Generates placeholder for IN clause
	     * @param  [array]	$array 	- array for in clause
	     * @return [string]
	     */
	    public static function placeholder( $array )
	    {
	    	return str_repeat( '?,' , count( $array ) - 1 ) . '?';
	    }
	}
?>