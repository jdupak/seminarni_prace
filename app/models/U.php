<?php 

	/**
	 * Utilities
	 */
	class U
	{
		/**
		 * Automatic actions
		 * @return [action]
		 */
		public static function auto( $context )
		{
			U::autoErrMsg( $context );
			U::smartyDebugger( $context );
			$context->checkAuthority();
		} 

		/**
		 * Display error message on frontend
		 * @param  [enum]	$type [info,warning,error]
		 * @param  [string]	$msg [info,warning,error]
		 * @return [action]
		 */
		public static function errMsg( $type, $msg, $context )
		{
			if ($type == "info" || $type == "warning" || $type == "error") {
				$type = ($type == "error") ? "danger" : $type;
				$singleMsg['msg'] = $msg;
				$singleMsg['type'] = $type;

				$context->smarty->append( 'errorMessage', $singleMsg );
			} else {
				throw new Exception("Error message type not supported (function U::errMsg)");
			}
		}

		/**
		 * Handle error message from GET
		 * @return [action]
		 */
		public static function autoErrMsg( $context )
		{
			if ( isset($_GET['msg']) ) {		// Error message
				if ( isset($_GET['et']) ) { 	// Error type
					U::errMsg($_GET['et'], urldecode($_GET['msg']), $context);
				} else {
					U::errMsg( 'error', urldecode($_GET['msg']), $context );
				}
			}
		}

		/**
		 * Display smarty debugger
		 * @return [action]
		 */
		public static function smartyDebugger( $context )
		{
			if ( DEV_MODE && isset($_GET['debug']) ) {
				$context->smarty->assign( 'debug' , true );
			}
		} 
	}


 ?>