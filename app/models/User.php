<?php 

	/**
	 * Class
	 *
	 */
	class User
	{
		static public $username = 'guest';
		static public $stat = false;
		static public $role = 0;

		static public function init()
		{
			if ( isset( $_SESSION['user'] ) ) {
				$data = self::chechSession();
				self::$username = $data['username'];
				self::$stat = true;
				self::$role = $data['role'] ?? 0;
			}
		}

		static public function chechSession()
		{
			$q = "SELECT users.username, userrole.role
					FROM sessions
					LEFT JOIN users
					ON users.id = sessions.user
					LEFT JOIN userrole
					ON users.id = userrole.user
					WHERE hash = ?
					AND TIMESTAMPDIFF(SECOND,timestamp,CURDATE()) < ?
					LIMIT 1;";
			$param[] = $_SESSION['user'];
			$param[] = SESSION_MAX_AGE;

			return Db::queryOne( $q, $param );
		}
		
		static public function login( $username, $password )
		{
			$q = "SELECT users.id, users.password, userrole.role
					FROM users
					LEFT JOIN userrole
					ON users.id = userrole.user
					WHERE users.username = ?;";

			$param[] = $username;

			$data = Db::queryOne( $q, $param );
			
			if ( password_verify( $password, $data['password'] ) ) {
				self::createSession( $data['id'] );
				self::$username = $username;
				self::$stat = true;
				self::$role = $data['role'];
				return true;
			} else {
				return false;
			}
		}

		static public function logout()
		{
			if ( isset( $_SESSION['user'] ) ) {
				$q = "DELETE
						FROM sessions
						WHERE hash = ?;";
				$param[] = $_SESSION['user'];
				Db::edit( $q, $param );

				$_SESSION['user'] = NULL;
			}
			self::$stat = false;
		}

		static public function register( $username, $password, $email )
		{
			if ( !filter_var( $email, FILTER_VALIDATE_EMAIL ) OR empty( $password ) OR empty( $username ) ) {
				return false;
			}

			$q = "SELECT count(username)
					FROM Users
					WHERE username = :username;";

			$param1["username"] = $username;

			if( Db::count( $q, $param1 ) !== 0 ) {
				return;
			};

			$q = "SELECT count(email)
					FROM Users
					WHERE email = :email;";

			$param2["email"] = $email;
			if( Db::count( $q, $param2 ) !== 0 ) {
				return;
			};
			$q = "INSERT INTO users 
					(`id`, `username`, `password`, `email`)
					VALUES (NULL, :username, :password, :email);";
			$param3["username"] = $username;
			$param3["email"] = $email;
			$param3["password"] = password_hash( $password, PASSWORD_DEFAULT );
			Db::edit( $q,$param3 );
			$param4["user"] = Db::getLastId();

			$q = "INSERT INTO userrole
					(id, user, role)
					VALUES (NULL, :user, 1);";
			Db::edit( $q,$param4 );
		}

		static public function checkAuthority( $requiredClearance = 0 )
		{
			if ( isset( self::$role ) ) {
				return (self::$role >= $requiredClearance);
			} elseif ( $requiredClearance = 0 ) {
				return true;
			} else {
				return false;
			}
		}	

		static public function createSession( $userID )
		{
			$q = "INSERT INTO `sessions`(`user`, `hash`, `timestamp`)
					VALUES (?,?,NULL)
					ON DUPLICATE KEY
					UPDATE hash = ?, timestamp = CURRENT_TIMESTAMP()";
			$param2[] = $userID;
			$param2[] = sha1($userID + 'w*8R/t79' + time());
			$_SESSION['user'] = $param2[] = $param2[1];
			Db::edit( $q, $param2 );
		}
	}

 ?>