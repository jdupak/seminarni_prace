<?php 

	/**
	 * Class of a cocktail object
	 */
	class Cocktail
	{
		public $id;
		public $name;
		public $altNames; // ARRAY || STRING
		public $category = Array();
		public $img;
		public $glass;
		public $volume;
		
		public $serving;
		public $procedure;
		public $description;
		public $dateOfCreation;

		public $recipe = Array();

		/**
		 * Load data from DB and form cocktail object
		 * 
		 * @param  [int] $id - Cocktail id
		 * @param  [bool] $raw - Return altnames as is
		 * @return [object] Cocktail object
		 */
		public function load( $id, $raw = false )
		{
			//Check validity of id (number) and eventualy replace with default
			$this->id = is_numeric( $id ) ? $id*1 : 0;

			// Set variables to by added to query by PDO
			$params["id"] = $this->id;

			/**
			 * Load basic data from DB and inject them directly to this object
			 */
			$q = "SELECT name, altNames, img, (
						SELECT name
						FROM Categories
						WHERE Categories.id=Cocktails.glass)
					AS `glass`,	`volume`, `serving`, `procedure`, description, `date`
					FROM Cocktails
					WHERE Cocktails.id = :id;";
			Db::queryObject( $q, $this, $params );

			if ( $raw ) {
				// Semicolon separated format to array
				$this->altNames = explode( ';', $this->altNames );
			}
			
			/**
			 * Load categories of cocktail
			 */
			$q = "SELECT Categories_groups.name as groupname, Categories.name
					FROM Categories_links
					LEFT JOIN Categories ON Categories_links.category=Categories.id
					LEFT JOIN Categories_groups ON Categories.group=Categories_groups.id
					WHERE cocktail = :id;";

			$this->category = Db::query( $q, $params, "assoc", "group" );

			/**
			 * Load recipe of this cocktail
			 */
			$q = "SELECT Ingredients.id, Ingredients.name, Recipes.amount, Ingredients.package as package, Ingredients.mu, Ingredients.price, Ingredients.shop, Ingredients.link,Ingredients.id2
					FROM Recipes
					LEFT JOIN Ingredients ON Recipes.ingredient=Ingredients.id
					WHERE Recipes.cocktail = :id;";

			$this->recipe = Db::query( $q, $params, "assoc" );
		}

		/**
		 * Insert new cocktail to DB
		 * 
		 * @param  [array] $data
		 * @return [action] Write to DB
		 */
		public function insert( $data = Array() )
		{
			$q = "INSERT INTO `cocktails`(`id`, `name`, `altNames`, `img`, `glass`, `volume`, `serving`, `procedure`, `description`, `date`)
					VALUES (NULL, :name, :altNames, :img, :glass, :volume, :serving, :procedure, :description, now() );";
			
			Db::edit( $q, $data[0] );
			$this->id = Db::getLastId();

			$q = "INSERT INTO `recipes`(`id`, `cocktail`, `ingredient`, `amount`)
					VALUES (NULL, :cocktail, :ingredient, :amount);";
			
			foreach ( $data[1] as $recipeRow ) {
				$recipeRow['cocktail'] = $this->id;
				Db::edit( $q, $recipeRow );
			}
			
			return $this->id;
		}

		/**
		 * Edit cocktail detail
		 * 
		 * @param  [int] $id   [description]
		 * @param  [array] $data [description]
		 * @return [null]
		 */
		public function update( $id,$data )
		{
			/**
			 * Update basic data
			 */
			$data[0]['id'] = (int)$id;

			$q = "UPDATE `cocktails`
					SET `name`= :name, `altNames`= :altNames, `img`= :img, `glass`= :glass, `volume`= :volume, `serving`= :serving, `procedure`= :procedure, `description`= :description, `date`= now()
					WHERE `id`= :id;";
			Db::edit( $q, $data[0] );

			/**
			 * Update recipe
			 * - get contenporary rows ids
			 * - modify them
			 * - create new rows when necessary
			 * - delete extra rows if necessary
			 */
			$q = "SELECT id
					FROM Recipes
					WHERE Recipes.cocktail = :id;";

			$data[2]['id'] = $data[0]['id'];
			$recipeRows = Db::query( $q, $data[2], "assoc" );

			$q = "UPDATE `recipes`
					SET Recipes.cocktail = :cocktail, Recipes.ingredient = :id, Recipes.amount = :amount
					WHERE Recipes.id= :row;";
			foreach ( $data[1] as $i => $row ) {
		        if ( isset($recipeRows[0]) ) {
					$row['cocktail'] = $data[0]['id'];
					$row['row'] = $recipeRows[0]['id'];
					Db::edit( $q, $row );
					array_splice( $recipeRows, 0, 1 );
					unset( $data[1][$i] );
				} else {
					$q = "INSERT INTO `recipes`(`id`, `cocktail`, `ingredient`, `amount`)
							VALUES (NULL, :cocktail, :id, :amount);";
					
						$row['cocktail'] = $data[0]['id'];
						Db::edit( $q, $row );
				}
			}

			if ( isset( $recipeRows[0] ) ) {
				$q = "DELETE FROM `recipes` WHERE `id` IN(" . implode( ',' , $recipeRows[0] ) . ");";
					Db::edit( $q );
			}
		}

		public function delete( $id )
		{
			$params["id"] = $id;
			$q[] = "DELETE FROM `recipes` WHERE `cocktail` = :id;";
			$q[] = "DELETE FROM `categories_links` WHERE `cocktail` = :id;";
			$q[] = "DELETE FROM `cocktails` WHERE `id` = :id;";
			
			$stat = 0;

			$stat += Db::edit( $q[0], $params );


			foreach ( $q as $query ) {
				$stat += Db::edit( $query, $params );
			}

			return $stat;
		}

		/**
		 * Cocktails listing
		 * @return [array] cocktails data with categories as subarray
		 */
		public function list()
		{
			$data[] = $this->listingParams['n'] = $_GET['n'] ?? 20; 
			$this->listingParams['page'] = ( ( $_GET['page'] ?? 1 ) - 1);
			$data[] = $this->listingParams['page'] * $this->listingParams['n'];
			$orderBy = $this->listingParams['orderby'] = $_GET['orderby'] ?? "name";
			$order = $this->listingParams['order'] = ( ( $_GET['order'] ?? "asc" ) == "desc" ) ? "DESC" : "ASC";
			$cat = $this->listingParams['categories'] = $_GET['cat'] ?? [];
			$data2 = [];

			if ( !empty( $cat ) ) {
				$placeholder  = str_repeat( '?,' , count( current( $cat ) ) - 1 ) . '?';
				$q = "SELECT Cocktails.id, Cocktails.id, Cocktails.name, Cocktails.altNames, Cocktails.img, Cocktails.date
						FROM Cocktails
						WHERE Cocktails.id IN (
							SELECT Categories_links.cocktail
							FROM Categories_links
							WHERE Categories_links.category IN ({$placeholder}) 
						)";
				$q2 = "SELECT count(*)
						FROM Cocktails
						WHERE Cocktails.id IN (
							SELECT Categories_links.cocktail
							FROM Categories_links
							WHERE Categories_links.category IN ({$placeholder}) 
						)";
				for ($i=2; $i <= count( $cat ); $i++) {
					$placeholder  = str_repeat( '?,' , count( next( $cat ) ) - 1 ) . '?';
					$part .= " AND Cocktails.id IN (
							SELECT Categories_links.cocktail
							FROM Categories_links
							WHERE Categories_links.category IN ({$placeholder}) 
							)";
					$q .= $part;
					$q2 .= $part;
				}
				$q .= " ORDER BY {$orderBy} {$order}
						LIMIT ? OFFSET ?;";
				$data = array_merge( call_user_func_array('array_merge', $cat), $data );
				$data2 = call_user_func_array('array_merge', $cat);
			} else {
				$q = "SELECT Cocktails.id, Cocktails.id, Cocktails.name, Cocktails.altNames, Cocktails.img, Cocktails.date
						FROM Cocktails
						ORDER BY {$orderBy} {$order}
						LIMIT ? OFFSET ?;";
				$q2 = "SELECT count(*)
						FROM Cocktails;";

			}
			$cocktailsData = Db::query( $q, $data, "assoc", "unique" );
			$this->listingParams['count'] = Db::count( $q2, $data2 );
			return $cocktailsData;
		}

		public function listCategories()
		{
			$q = "SELECT categories_groups.id as category_id, categories_groups.name as category_name, categories.id, categories.name
					FROM categories
					LEFT JOIN categories_groups
					ON categories.group = categories_groups.id;";
			$data = Db::query( $q, [], 'assoc', '');
			//Make part of PDO
			foreach ($data as $key => $value) {
				$result[$value['category_id']]['name'] = $value['category_name'];
				$result[$value['category_id']][$value['id']] = $value['name'];
			}
			return $result;
		}

		public function listCocktailCategories( $id )
		{
			$q = "SELECT categories.id
					FROM categories_links
					LEFT JOIN categories
					ON categories.id = categories_links.category
					WHERE categories_links.cocktail = ?";
			return Db::query( $q, [$id], 'num', 'single');
		}

		public function deleteCategories( $id, $list )
		{
			if ( empty( $list ) ) { return; }
			$p =  Db::placeholder($list);
			$q = "DELETE FROM categories_links
					WHERE cocktail = ?
					AND category IN ({$p});";
			array_unshift( $list, $id );
			return Db::edit( $q, $list );
		}

		public function addCategories( $id, $list )
		{
			if ( empty( $list ) ) { return; }
			$q = "INSERT INTO categories_links (cocktail, category)
					VALUES (?,?);";
			$k = 0;
			foreach ($list as $category) {
				$stat = Db::edit( $q, [$id,$category] );
				$k++;
			}
			return ($stat == $k++);
		}
	}


 ?>