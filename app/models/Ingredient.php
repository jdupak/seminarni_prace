<?php 

	/**
	 * Class of a ingredient object
	 */
	class Ingredient
	{
		public $id;
		public $name;
		public $id2;
		public $package;
		public $mu;
		public $price;
		public $shop;
		public $link;

		/**
		 * Load one ingredient
		 * @param  [int] $id [description]
		 * @return [NULL] data saved to object
		 */
		public function load( $id )
		{
			$q = "SELECT id,name,id2,package,mu,price,shop,link 
				FROM Ingredients
				WHERE id = :id;";

			$params["id"] = $id;
			$x = Db::queryObject( $q, $this, $params );
		}

		/**
		 * Load multiple ingredients
		 * @param  array  $ids
		 * @return [array] associative array by id
		 */
		public function loadMultiple( $ids = array() )
		{
			$in  = str_repeat('?,', count($ids) - 1) . '?';
			$q = "SELECT id,name,id2,package,mu,price,shop,link 
				FROM Ingredients
				WHERE id IN ({$in});";
			return Db::query( $q, $ids, "assoc" );
		}

		/**
		 * Load all ingredients
		 * @return [array] associative array by id
		 */
		public function loadAll()
		{
			$q = "SELECT id,name,id2,package,mu,price,shop,link 
				FROM Ingredients;";
			return Db::query( $q, [], "assoc" );
		}

		/**
		 * Load alternative ingredients to given product
		 * @param  [int]  $id
		 * @return [array] associative array by id
		 */
		public function variants()
		{
			$q = "SELECT id,name,id2,package,mu,price,shop,link 
				FROM Ingredients
				WHERE id2 = :id;";

			$params["id"] = $id;
			return Db::query( $q, $ids, "assoc" );
		}

		/**
		 * Load basic ingredients, which are not alternatives
		 * to other products
		 * @param  [int]  $id
		 * @return [array] associative array by id
		 */
		public function variantOptions()
		{
			$q = "SELECT id,name,package,mu,price,shop
				FROM Ingredients
				WHERE id2 IS NULL;";

			return Db::query( $q, [], "assoc" );
		}
		
		/**
		 * Insert new ingredient
		 * @param  [array] $data 	- New data
		 * @return [int] new item id
		 */
		public function insert( $data )
		{
			$data['id2'] = NULL;
			$data['price'] ?? NULL;
			$data['shop'] ?? NULL;
			$data['link'] ?? NULL;


			$q = "INSERT INTO `ingredients`(`id`, `name`, `id2`, `package`, `mu`, `price`, `shop`, `link`) VALUES (NULL, :name, :id2, :package, :mu, :price, :shop, :link);";
			Db::edit( $q, $data );
			return Db::getLastId();
		}
		
		/**
		 * Edit ingredient
		 * @param  [int] $id
		 * @param  [array] $data 	- New data
		 * @return [int] number of edited rows
		 */
		public function edit( $id, $data )
		{
			$q = "UPDATE ingredients
				SET name = :name, id2 = :id2, package = :package, mu = :mu, price = :price, shop = :shop, link = :link
				WHERE id = :id;";
			$data['id'] = $id;
			Db::edit( $q, $data );
		}

		/**
		 * Delete ingredient
		 * @param  [int] $id
		 * @return [int] number of deleted rows
		 */
		public function delete( $id )
		{
			$q = "DELETE FROM Ingredients WHERE id = :id;";
			$params["id"] = $id;
			return Db::edit( $q, $params );
		}

		/**
		 * TODO
		 */
		public function updatePrice()
		{
			# parse url if is listed source
			# chose source
			# download page
			# parse
			# update price
		}

	}


 ?>