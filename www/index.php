<?php
error_reporting( 0 );
ini_set('display_errors', 0);
date_default_timezone_set ( "Europe/Prague" );
mb_internal_encoding("UTF-8");
mb_regex_encoding("UTF-8");
setlocale(LC_CTYPE, 'en_US.UTF-8');
define('CORE_DIR', dirname( dirname(__FILE__) ) . DIRECTORY_SEPARATOR);

session_start();

require_once( CORE_DIR . 'app' . DIRECTORY_SEPARATOR . 'utils.php');
require_once( CORE_DIR . 'app' . DIRECTORY_SEPARATOR . 'libs' . DIRECTORY_SEPARATOR . 'smarty' . DIRECTORY_SEPARATOR . 'Smarty.class.php');

$smarty = new Smarty();

require_once( CORE_DIR . 'configs' . DIRECTORY_SEPARATOR . 'config.inc.php' );
require_once( CORE_DIR . 'app' . DIRECTORY_SEPARATOR . 'autoloader.php' );

$router = new RouterController( $smarty );
$router->init( $_SERVER['REQUEST_URI'] );

 ?>