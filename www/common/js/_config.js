/**
 * General JS config
 */

var config = {
	/**
	 * Develop mode
	 *
	 * Debugging & status report via console enabled
	 * @type {Boolean}
	 */
	dev: true,
};

// Constant protection
Object.freeze(config);