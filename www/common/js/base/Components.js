var base = base || {};

/**
 * Registrer components and list them
 *
 * @property {array} list	- list of components
 */
base.Components = function() {
    this.list = new Array;
    this.register = function(name, constructor) {
        this.list[name] = constructor;
    };
};