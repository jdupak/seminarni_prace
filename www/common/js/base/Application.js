var base = base || {};

/**
 * Initiate basic application
 * 
 * @param {object} componentFactory     - component listing object
 */
base.Application = function(componentFactory) {
    base.Application.componentFactory = componentFactory;
};

/**
 * Install componets to elements with data-components property
 * accoring to comonentFactory list of components
 */
base.Application.prototype.loadComponents = function() {

    var componentElements = $.find('*[data-component]');
    length = componentElements.length;

    
    if (length === 0) {
        log('No components found');
    }
    
    for (var i = 0; i < length; i++) {
        var name = $(componentElements[i]).attr('data-component');

        try {
            base.Application.prototype.loadComponent(componentElements[i], name);  
        } catch(err) {
            error('Unable to install ' + name + ' component');
            error(err);
        }
    }

    log('Components installation finished');

};

/**
 * Install component on HTML element
 * 
 * @param  {DOM object} element     - Element with data-component property
 */
base.Application.prototype.loadComponent = function(element, name) {
    componentConstructor = base.Application.componentFactory.list[name];

    if (componentConstructor instanceof Function === false) {
        warn("Component '" + name + "' is not registered");
        return;
    }

    var componentInstance = new componentConstructor(element);

    componentInstance.init(element);

    log('Installed:\n ',componentInstance, '\n    --element:', element);
};  