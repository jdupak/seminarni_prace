/**
 * Toggle content component
 * 
 * @requires  typehead https://twitter.github.io/typeahead.js/
 */
app.component.IngredientVariants = function()
{
};

app.component.IngredientVariants.prototype.init = function(element) {
	var ingredientVariants = new Bloodhound({
	  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
	  queryTokenizer: Bloodhound.tokenizers.whitespace,
	  prefetch: '/data/ingredient-variants.json'
	});

	$(element).typeahead(null,{
	  name: 'ingredientVariants',
	  display: 'id',
	  source: ingredientVariants,
	  templates: {
	    suggestion: function(data) {
	    	if (data.shop === null ) { data.shop = ""; }
		    return '<p><strong>' + data.name + '</strong> – ' + data.package + data.mu + " " + data.shop + '</p>';
		}
	  }
	});
};