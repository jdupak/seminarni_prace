/**
 * Toggle content component
 * 
 * @requires  magnific popup
 */
app.component.Popup = function()
{
};

app.component.Popup.prototype.init = function(element) {
	$(element).find('.Popup-control').magnificPopup({
	    type:'inline',
	    midClick: true
	});
};