/**
 * Toggle content component
 * 
 * @requires  bootstrap/collapse
 */
app.component.Collapse = function()
{
};

app.component.Collapse.prototype.init = function(element) {
	control = $(element).children('.Collapse-control');
	target = $(element).children('.Collapse-item');

	log('x');

	target.addClass('collapse');

	log('x');
	
	control.on('click', function(event) {
		target.collapse('toggle');
	});
};