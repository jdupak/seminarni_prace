/**
 * Recipe table
 * 
 * @requires  jQuery
 */
app.component.RecipeTable = function()
{
};

app.component.RecipeTable.prototype.init = function(element) {
    this.element = element;

    // Ignore row with headers
    rows = $(element).find('tr').slice(1);

    $(element).after(this.template.masterAddButton);

    rows.append(this.template.addButton);
    rows.append(this.template.delButton);

    $(element).find('.js-create-ingredient').on('click', this.createIngredient.bind(this));
    $(element).find('.Delete').on('click', this.deleteRow.bind(this));
    $(element).find('.Add').on('click', this.addRow.bind(this));
    $(element).parent().find('.MasterAdd').on('click', this.appendRow.bind(this));
};

app.component.RecipeTable.prototype.addRow = function() {
    tr = $(event.target).closest('tr');
    tr.before(this.template.row);
    newTr = tr.prev('tr');
    newTr.append(this.template.addButton).append(this.template.delButton);
    newTr.find('.Add').on('click', this.addRow.bind(this));
    newTr.find('.Delete').on('click', this.deleteRow.bind(this));
    this.renumberRows();
    newTr.find('.js-create-ingredient').on('click', this.createIngredient.bind(this));
    Application.loadComponent(newTr.find('*[data-component="ingredients"]'),"ingredients");
};

app.component.RecipeTable.prototype.appendRow = function() {
    table = $(event.target).prev('table').children('tbody');
    rowNr = Number(table.children('tr').last().attr('data-row-nr')) + 1;
    table.append(this.template.row);
    lastTr = table.children('tr').last();
    lastTr.append(this.template.addButton).append(this.template.delButton);
    lastTr.attr('data-row-nr', rowNr);
    lastTr.find('.Add').on('click', this.addRow.bind(this));
    lastTr.find('.Delete').on('click', this.deleteRow.bind(this));
    this.renumberRows();
    Application.loadComponent(lastTr.find('*[data-component="ingredients"]'),"ingredients");
    lastTr.find('.js-create-ingredient').on('click', this.createIngredient.bind(this));

};

app.component.RecipeTable.prototype.deleteRow = function() {
    $(event.target).closest('tr').remove();
};

app.component.RecipeTable.prototype.renumberRows = function() {
    rows = $(this.element).find('tr');
    for (var i = 0; i < rows.length; i++) {
        row = $(rows[i]);
        row.attr('data-row-nr', i);
        row.find('.f_name').attr('name', 'f_recipe_[' + i + '][name]');
        row.find('.f_id').attr('name', 'f_recipe[' + i + '][id]');
        row.find('.f_a').attr('name', 'f_recipe[' + i + '][amount]');
    }
};

app.component.RecipeTable.prototype.createIngredient = function() {
    var row = $(event.target).closest('tr');
    var n = row.attr('data-row-nr');
    row.find('.f_price').html('<input type="text" name="f_ingredient['+n+'][price]">');
    row.find('.f_package').html('<input type="text" name="f_ingredient['+n+'][package]">');
    row.find('.f_mu').html('<input type="text" name="f_ingredient['+n+'][mu]">');
    row.find('.f_shop').html('<input type="text" name="f_ingredient['+n+'][shop]">');
    row.find('.f_link').closest('td').html('<input type="text" name="f_ingredient['+n+'][link]">');
};

/**
 * HTML templates for appending
 */
app.component.RecipeTable.prototype.template = {};

app.component.RecipeTable.prototype.template.delButton = $.trim('\
    <td><a class="Button Button--warning Delete">X</a></td>\
');

app.component.RecipeTable.prototype.template.addButton = $.trim('\
    <td><a class="Button Button--warning Add">+</a></td>\
');

app.component.RecipeTable.prototype.template.masterAddButton = $.trim('\
    <a class="Button Button--warning MasterAdd">Přidat nový řádek na konec</a>\
');

app.component.RecipeTable.prototype.template.row = $.trim('\
    <tr>\
        <td><input type="text" class="f_name" data-component="ingredients">\
        <input type="hidden" class="f_id"></td>\
        <td><input type="number" class="f_a" step="0.001"></td>\
        <td class="f_package"></td><td class="f_mu"></td><td class="f_price"></td><td class="f_shop"></td><td class="f_link"><a href=""></a></td>\
        <td><span class="Button js-create-ingredient">*</span></td>\
    </tr>\
');