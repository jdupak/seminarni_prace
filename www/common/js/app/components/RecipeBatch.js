/**
 * Toggle content component
 * 
 * @requires  jQuery
 */
app.component.RecipeBatch = function()
{
};

app.component.RecipeBatch.prototype.init = function(element) {
	this.priceOne = $(element).find('.js-price-one');
	this.clonePriceOne = this.priceOne.slice(1);
	this.inputsVolume = $(element).find('.js-volume');
	this.cloneInputsVolume = this.inputsVolume.slice(1);
	this.inputsQuantity = $(element).find('.js-quantity');
	this.cloneInputsQuantity = this.inputsQuantity.slice(1);
	this.inputs = $.merge(this.inputsVolume, this.inputsQuantity);
	this.element = $(element);

	this.count();

	this.clonePriceOne.css('visibility','hidden');
	this.cloneInputsVolume.css('visibility','hidden');
	this.cloneInputsQuantity.css('visibility','hidden');

	this.inputs.on('change', this.changeEv.bind(this));
};

app.component.RecipeBatch.prototype.changeEv = function(event) {
	event.preventDefault();
	this.cloneInputsVolume.val(this.inputsVolume.first().val());
	this.cloneInputsQuantity.val(this.inputsQuantity.first().val());
	this.count();
};

app.component.RecipeBatch.prototype.count = function() {
	var rows = this.element.find('tr').splice(1);
	var priceOne = 0;
	$(rows).each(function(index, tr) {
		var volume = $(tr).find('.js-volume').val();
		var quantity = $(tr).find('.js-quantity').val();
		var amountPerL = $(tr).find('.js-amount-per-l').text();
		var package = $(tr).find('.js-package').text();
		var priceMu = $(tr).find('.js-price-mu').text();

		var muAmountRaw = amountPerL * volume / package * quantity;
		var muAmount = Math.ceil(muAmountRaw);
		var amountSum = amountPerL * volume * quantity;
		var priceSum = muAmount * priceMu;
		var pricePerOne = priceSum / quantity;
		priceOne += pricePerOne;

		$(tr).find('.js-price-per-one').text(round(pricePerOne,2));
		$(tr).find('.js-price-sum').text(round(priceSum,2));
		$(tr).find('.js-mu-quantity').text(muAmount);
		$(tr).find('.js-mu-quantity-raw').text(round(muAmount,5));
		$(tr).find('.js-amount-sum').text(round(amountSum,2));
	});
	this.element.find('.js-price-one').text(round(priceOne,2));
};