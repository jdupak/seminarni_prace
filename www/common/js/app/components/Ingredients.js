/**
 * Toggle content component
 * 
 * @requires  typehead https://twitter.github.io/typeahead.js/
 */
app.component.Ingredients = function()
{
};

app.component.Ingredients.prototype.init = function(element) {
	var ingredients = new Bloodhound({
	  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
	  queryTokenizer: Bloodhound.tokenizers.whitespace,
	  prefetch: '/data/ingredient-list.json'
	});

	$(element).typeahead({
	  hint: true,
	  highlight: true
	},
	{
	  name: 'ingredients',
	  display: 'name',
	  source: ingredients
	});

	$(element).bind('typeahead:select', function(ev, selected) {
	  var row = $(element).closest('tr');
	  log(selected);
	  log(row.find('.f_price'));
	  row.find('.f_id').val( selected.id );
	  row.find('.f_price').text( selected.price );
	  row.find('.f_mu').text( selected.mu );
	  row.find('.f_package').text( selected.amount );
	  row.find('.f_shop').text( selected.shop );
	  row.find('.f_link').html('<a href="'+selected.link+'">'+selected.link+'</a>');
	  row.find('.f_id').text( selected.id );
	});
};