/**
 * Tabs
 * 
 * @requires  jQuery
 */
app.component.Tabs = function()
{	
};

app.component.Tabs.prototype.init = function(element) {
	controls = $(element).find('.Tabs-control');
	items = $(element).find('.Tabs-item');
	items.show().hide();

	suburl = window.location.hash;

	if ($('.Tabs-item'+suburl).length == 1) {
		$('.Tabs-item'+suburl).show();
	}
	else {
	 	first = $(element).attr('data-setting-first-on');

		if (first == "true") {
			items.first().show();		
		}
	}

	controls.on('click', this.changeTab.bind(this));
};

app.component.Tabs.prototype.changeTab = function(event) {
	event.preventDefault();
	click = $(event.target);
	target = $('#'+click.attr('data-target'));
	items.hide();
	target.fadeIn(500);

	// Change #location without jumping there (for bookmarking etc.)
	if(history.pushState) {
	    history.pushState(null, null, click.attr('href'));
	}
	// Fallback
	else {
	    location.hash = click.attr('href');
	}
};