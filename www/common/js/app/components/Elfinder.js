/**
 * File manager
 * 
 * @requires  elFinder
 */
app.component.Elfinder = function()
{
};

app.component.Elfinder.prototype.init = function(element) {
	this.element = element;
	$(element).on('dblclick', this.open.bind(this));
};

app.component.Elfinder.prototype.open = function(event) {
	var elfinder = window.open("/elfinder/index.html", "File Manager", "width=800,height=400");
	var file = null;
	elfinder.element = this.element;
	elfinder.onbeforeunload = function(){
		file = elfinder.selectfile;
		$(elfinder.element).val(file);
	};
};