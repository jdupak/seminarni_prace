var app = app || {};

/**
 * Controller of JS application
 * 
 * @param  {object} window - Browser window object
 * @param  {object} base   - Basic classes
 * @param  {object} app    - Main application object
 */
(function(window, base, app, $) {
	/**
	 * Components wrapper
	 * @type {object}
	 */
	app.component =  app.component || {};
	
	$(document).ready(function($) {

		/**
		 * Componets registration object
		 * 
		 * @type {object}
		 * @instance
		 *
		 * @method registrer('name',constructor)
		 */
		components = new base.Components;
		components.register('collapse', app.component.Collapse);
		components.register('elfinder', app.component.Elfinder);
		components.register('ingredients', app.component.Ingredients);
		components.register('ingredient-variants', app.component.IngredientVariants);
		components.register('tabs', app.component.Tabs);
		components.register('popup', app.component.Popup);
		components.register('recipe-batch', app.component.RecipeBatch);
		components.register('recipe-table', app.component.RecipeTable);
		
		/**
		 * Active application controlling object
		 * 
		 * @type {object}
		 * @instance
		 */
		Application = new base.Application(components);
		Application.loadComponents();	
	});

})(window, base, app, jQuery);