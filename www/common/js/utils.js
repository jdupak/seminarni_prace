/**
 * Various functions with global scope
 */

/**
 * Object inharitance util
 * 
 * @param  {object} child
 * @param  {object} parent
 * @return extend this with parent but do not overwrite child
 */
function extend(child, parent) {
    var F = function() {};
    F.prototype = parent.prototype;
    child.superClass_ = parent.prototype;
    child.prototype = new F();
    child.prototype.constructor = child;
}

/**
 * Console logging only in dev mode
 * 
 * @param  {string} content
 * @return console.log()
 */
function log() {
	if (config.dev) {
		console.log.apply(console, arguments);
	}
}
/**
 * Console logging only in dev mode
 * 
 * @param  {string} content
 * @return console.log()
 */
function warn() {
	if (config.dev) {
		console.warn.apply(console, arguments);
	}
}
/**
 * Console logging only in dev mode
 * 
 * @param  {string} content
 * @return console.log()
 */
function error() {
	if (config.dev) {
		console.error.apply(console, arguments);
	}
}

/**
 * Reduces time of event firiings to in in given interval
 * @param  {func} func
 * @param  {int} wait 		- Time in miliseconds
 * @param  {[type]} immediate - If `immediate` is passed, trigger
 *  the function on the leading edge, instead of the trailing.
 * @return {func}
 *
 * @author  http://underscorejs.org/
 */
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
}

/**
 * Round number to n decimals
 * @param  {number} value   
 * @param  {number} decimals
 * @return {number}         
 */
function round(value, decimals) {
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}