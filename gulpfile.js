// Load Gulp
var gulp = require('gulp'),
    gutil = require('gulp-util'),
    livereload = require('gulp-livereload');
    plugins = require('gulp-load-plugins')({
        rename: {
            'gulp-live-server': 'serve'
        }
    });
    sort = require('gulp-sort');

const stripCssComments = require('gulp-strip-css-comments');

// Start Watching: Run "gulp"
gulp.task('default', ['watch']);

// Minify Custom JS: Run manually with: "gulp scripts"
gulp.task('scripts', function () {
    return gulp.src(['www/common/js/*.js','www/common/js/base/*.js','www/common/js/app/**/*.js','www/common/js/app/*.js'])
        .pipe(plugins.jshint('.jshintrc'))
        .pipe(plugins.jshint.reporter('jshint-stylish'))
        .pipe(plugins.uglify({
            output: {
                'ascii_only': true
            }
        }))
        .pipe(plugins.concat('scripts.min.js'))
        .pipe(gulp.dest('www/common/build'));
});
gulp.task('scripts-dev', function () {
    return gulp.src(['www/common/js/*.js','www/common/js/base/*.js','www/common/js/app/**/*.js','www/common/js/app/*.js'])
        .pipe(plugins.concat('scripts.min.js'))
        .pipe(gulp.dest('www/common/build'));
});

gulp.task('vendor-scripts', function () {
    return gulp.src(['www/common/js/vendor/**/*.js'])
        .pipe(sort())
        .pipe(plugins.uglify({
            output: {
                'ascii_only': true
            }
        }))
        .pipe(plugins.concat('vendor.min.js'))
        .pipe(gulp.dest('www/common/build'));
});

// Less to CSS: Run manually with: "gulp styles"
gulp.task('styles', function () {
    return gulp.src('www/common/less/main.less')
        .pipe(plugins.plumber())
        .pipe(plugins.less())
        .on('error', function (err) {
            gutil.log(err);
            this.emit('end');
        })
        .pipe(plugins.autoprefixer({
            browsers: [
                    '> 1%',
                    'last 2 versions',
                    'firefox >= 4',
                    'safari 7',
                    'safari 8',
                    'IE 8',
                    'IE 9',
                    'IE 10',
                    'IE 11'
                ],
            cascade: false
        }))
        .pipe(stripCssComments({
            preserve: false
        }))
        .pipe(plugins.cssmin())
        .pipe(plugins.concat('styles.min.css'))
        .pipe(gulp.dest('www/common/build')).on('error', gutil.log);
});

// Default task
gulp.task('watch', function () {
    gulp.start(['styles']);
    gulp.start(['scripts']);
    gulp.watch('www/common/js/app/*.js', ['scripts']);
    gulp.watch('www/common/js/**/*.js', ['scripts']);
    gulp.watch('www/common/js/**/**/*.js', ['scripts']);
    gulp.watch('www/common/js/app/*.js', ['scripts']);
    gulp.watch('www/common/js/vendor/*.js', ['vendor-scripts']);
    gulp.watch('www/common/less/**/*.less', ['styles']);
});

var browserSync = require('browser-sync').create();

gulp.task("browser-sync", ["watch"], function ()
{
    browserSync.init({
        proxy: "seminarka.jd"
    });
    browserSync.reload();
});