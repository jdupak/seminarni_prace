<?php

/**
 * @todo Předunout do DB ???
 */

define('E404_TITLE', "Chyba 404");
define('HP_TITLE', "TheMixingDB");
define('DETAIL_TITLE', "TheMixingDB");
define('EDIT_TITLE', "TheMixingDB");
define('DB_ERROR', "Nelze se připojit k databázi");
define('AUTOLOADER_NOT_FOUND', "Autoloader nenalezl požadovaný soubor");
define('DELETE_ERR', "Záznam nebylo možné odstranit");
define('PARAM_ERR', "nesprávný parameter");
define('CREATE_SUCCESS', "Záznam byl úspěšně vytvořen");
define('EDIT_SUCCESS', "Záznam byl úspěšně upraven");
define('CREATE_FAILURE', "Záznam nebyl vytvořen");
define('DELETE_SUCCESS', "Záznam byl úspěšně odstraněn");
define('NO_RECORD', "žádný záznam");
define('FOREIGN_KEY_ERR', "záznam je vázán na další záznamy");
define('NOT_ENOUGH_ARGUMENTS', "zadali jste málo argumentů");
define('NOT_FOUND', "Záznam nenalezen");
define('ACCESS_DENIED', "Přístup odepřen");
define('LOGIN', "Login");
define('WELCOME', "Vítejte");
define('LOGIN_FAILURE', "Uživatelské jméno nebo heslo není správné");
define('LOGOUT_SUCCESS', "Uživatel byl odhlášen");

 ?>