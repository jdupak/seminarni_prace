<?php 
// ==============================================
//               CONFIGURATION
// ==============================================

// Error & encoding settings
// ==============================================
error_reporting( E_ALL );
ini_set('display_errors', 1);
date_default_timezone_set ( "Europe/Prague" );
mb_internal_encoding("UTF-8");
mb_regex_encoding("UTF-8");
setlocale(LC_CTYPE, 'en_US.UTF-8');

// Local settings
// ==============================================
define( 'DEV_MODE' , true );

// Directory variables
// ==============================================
define( 'APP_DIR', CORE_DIR . 'app' . DIRECTORY_SEPARATOR );
define( 'TEMPLATE_DIR', CORE_DIR . 'templates' . DIRECTORY_SEPARATOR );
define( 'CONFIG_DIR', CORE_DIR . 'configs' . DIRECTORY_SEPARATOR );
define( 'UPLOAD_DIR', dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR);
define( 'IMAGES_DIR', UPLOAD_DIR . 'images' . DIRECTORY_SEPARATOR);

define( 'ROOT_URL', (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST']);


// Database login
// ==============================================
define( 'DB_HOST' , "localhost" );
define( 'DB_NAME' , "seminarni_prace" );
define( 'DB_USER' , "root" );
define( 'DB_PSWD' , "" );

// Project settings
// ==============================================
define( 'SESSION_MAX_AGE' , "900" ); // seconds
define( 'AUTHORITY_DELETE' , 5 ); // seconds

// Smarty template engine config
// ==============================================
$smarty->setTemplateDir( TEMPLATE_DIR );
$smarty->setCompileDir( CORE_DIR . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . 'templates_c' . DIRECTORY_SEPARATOR );
$smarty->setConfigDir( CONFIG_DIR );
$smarty->setCacheDir( CORE_DIR . 'cache' . DIRECTORY_SEPARATOR );

// Other config files
// ==============================================
require_once( CONFIG_DIR . "webtexts.config.php");
require_once( CONFIG_DIR . 'reroutings.config.php' );

 ?>