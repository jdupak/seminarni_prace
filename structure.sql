-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Stř 21. bře 2018, 17:24
-- Verze serveru: 5.7.14
-- Verze PHP: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `seminarni_prace`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `autorship`
--

CREATE TABLE `autorship` (
  `cocktail` int(10) UNSIGNED NOT NULL,
  `user` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(40) COLLATE latin2_czech_cs NOT NULL,
  `group` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `categories_groups`
--

CREATE TABLE `categories_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(40) COLLATE latin2_czech_cs NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `categories_links`
--

CREATE TABLE `categories_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `cocktail` int(10) UNSIGNED NOT NULL,
  `category` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `cocktails`
--

CREATE TABLE `cocktails` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(40) COLLATE latin2_czech_cs NOT NULL,
  `altNames` text COLLATE latin2_czech_cs,
  `img` text COLLATE latin2_czech_cs,
  `glass` int(10) UNSIGNED DEFAULT NULL,
  `volume` float DEFAULT NULL,
  `serving` text COLLATE latin2_czech_cs,
  `procedure` text COLLATE latin2_czech_cs,
  `description` text COLLATE latin2_czech_cs,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `cocktail` int(10) UNSIGNED NOT NULL,
  `link` text COLLATE latin2_czech_cs NOT NULL,
  `description` varchar(100) COLLATE latin2_czech_cs DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `ingredients`
--

CREATE TABLE `ingredients` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(40) COLLATE latin2_czech_cs NOT NULL,
  `id2` int(10) UNSIGNED DEFAULT NULL,
  `package` float NOT NULL,
  `mu` varchar(10) COLLATE latin2_czech_cs NOT NULL,
  `price` float UNSIGNED DEFAULT NULL,
  `shop` varchar(40) COLLATE latin2_czech_cs DEFAULT NULL,
  `link` text COLLATE latin2_czech_cs
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `rating`
--

CREATE TABLE `rating` (
  `id` int(10) UNSIGNED NOT NULL,
  `user` int(11) UNSIGNED NOT NULL,
  `cocktail` int(10) UNSIGNED NOT NULL,
  `rating` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `recipes`
--

CREATE TABLE `recipes` (
  `id` int(11) NOT NULL,
  `cocktail` int(10) UNSIGNED NOT NULL,
  `ingredient` int(10) UNSIGNED NOT NULL,
  `amount` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `sessions`
--

CREATE TABLE `sessions` (
  `username` varchar(20) COLLATE latin2_czech_cs NOT NULL,
  `hash` text COLLATE latin2_czech_cs NOT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `userrole`
--

CREATE TABLE `userrole` (
  `id` int(10) UNSIGNED NOT NULL,
  `user` int(11) UNSIGNED NOT NULL,
  `role` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `userroles`
--

CREATE TABLE `userroles` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` varchar(10) COLLATE latin2_czech_cs NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(20) COLLATE latin2_czech_cs NOT NULL,
  `password` varchar(32) COLLATE latin2_czech_cs NOT NULL,
  `email` varchar(50) COLLATE latin2_czech_cs NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

-- --------------------------------------------------------

--
-- Struktura tabulky `webtexts`
--

CREATE TABLE `webtexts` (
  `name` varchar(30) COLLATE latin2_czech_cs NOT NULL,
  `text` text COLLATE latin2_czech_cs NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin2 COLLATE=latin2_czech_cs;

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `autorship`
--
ALTER TABLE `autorship`
  ADD KEY `cocktail` (`cocktail`),
  ADD KEY `user` (`user`);

--
-- Klíče pro tabulku `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group` (`group`);

--
-- Klíče pro tabulku `categories_groups`
--
ALTER TABLE `categories_groups`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `categories_links`
--
ALTER TABLE `categories_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cocktail` (`cocktail`),
  ADD KEY `category` (`category`);

--
-- Klíče pro tabulku `cocktails`
--
ALTER TABLE `cocktails`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cocktail` (`cocktail`);

--
-- Klíče pro tabulku `ingredients`
--
ALTER TABLE `ingredients`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`),
  ADD KEY `cocktail` (`cocktail`);

--
-- Klíče pro tabulku `recipes`
--
ALTER TABLE `recipes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cocktail` (`cocktail`),
  ADD KEY `ingredient` (`ingredient`);

--
-- Klíče pro tabulku `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`username`);

--
-- Klíče pro tabulku `userrole`
--
ALTER TABLE `userrole`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`),
  ADD KEY `role` (`role`);

--
-- Klíče pro tabulku `userroles`
--
ALTER TABLE `userroles`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Klíče pro tabulku `webtexts`
--
ALTER TABLE `webtexts`
  ADD PRIMARY KEY (`name`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT pro tabulku `categories_groups`
--
ALTER TABLE `categories_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pro tabulku `categories_links`
--
ALTER TABLE `categories_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pro tabulku `cocktails`
--
ALTER TABLE `cocktails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT pro tabulku `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `ingredients`
--
ALTER TABLE `ingredients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pro tabulku `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `recipes`
--
ALTER TABLE `recipes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pro tabulku `userrole`
--
ALTER TABLE `userrole`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pro tabulku `userroles`
--
ALTER TABLE `userroles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `autorship`
--
ALTER TABLE `autorship`
  ADD CONSTRAINT `autorship_ibfk_1` FOREIGN KEY (`cocktail`) REFERENCES `cocktails` (`id`),
  ADD CONSTRAINT `autorship_ibfk_2` FOREIGN KEY (`user`) REFERENCES `users` (`id`);

--
-- Omezení pro tabulku `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`group`) REFERENCES `categories_groups` (`id`),
  ADD CONSTRAINT `categories_ibfk_2` FOREIGN KEY (`group`) REFERENCES `categories_groups` (`id`);

--
-- Omezení pro tabulku `categories_links`
--
ALTER TABLE `categories_links`
  ADD CONSTRAINT `categories_links_ibfk_2` FOREIGN KEY (`cocktail`) REFERENCES `cocktails` (`id`),
  ADD CONSTRAINT `categories_links_ibfk_3` FOREIGN KEY (`cocktail`) REFERENCES `cocktails` (`id`),
  ADD CONSTRAINT `categories_links_ibfk_4` FOREIGN KEY (`category`) REFERENCES `categories` (`id`);

--
-- Omezení pro tabulku `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_ibfk_1` FOREIGN KEY (`cocktail`) REFERENCES `cocktails` (`id`),
  ADD CONSTRAINT `images_ibfk_2` FOREIGN KEY (`cocktail`) REFERENCES `cocktails` (`id`);

--
-- Omezení pro tabulku `rating`
--
ALTER TABLE `rating`
  ADD CONSTRAINT `rating_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `rating_ibfk_2` FOREIGN KEY (`cocktail`) REFERENCES `cocktails` (`id`),
  ADD CONSTRAINT `rating_ibfk_3` FOREIGN KEY (`user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `rating_ibfk_4` FOREIGN KEY (`cocktail`) REFERENCES `cocktails` (`id`);

--
-- Omezení pro tabulku `recipes`
--
ALTER TABLE `recipes`
  ADD CONSTRAINT `recipes_ibfk_1` FOREIGN KEY (`cocktail`) REFERENCES `cocktails` (`id`),
  ADD CONSTRAINT `recipes_ibfk_2` FOREIGN KEY (`ingredient`) REFERENCES `ingredients` (`id`),
  ADD CONSTRAINT `recipes_ibfk_3` FOREIGN KEY (`cocktail`) REFERENCES `cocktails` (`id`),
  ADD CONSTRAINT `recipes_ibfk_4` FOREIGN KEY (`ingredient`) REFERENCES `ingredients` (`id`);

--
-- Omezení pro tabulku `userrole`
--
ALTER TABLE `userrole`
  ADD CONSTRAINT `userrole_ibfk_2` FOREIGN KEY (`role`) REFERENCES `userrole` (`id`),
  ADD CONSTRAINT `userrole_ibfk_3` FOREIGN KEY (`user`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `userrole_ibfk_4` FOREIGN KEY (`role`) REFERENCES `userroles` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
