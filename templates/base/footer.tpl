
</body>
    <script type="text/javascript" src="/common/build/vendor.min.js"></script>
{if isset($js)}
	<script type="text/javascript">
	var data = {$js|@json_encode};
	</script>
{/if}
    <script type="text/javascript" src="/common/build/scripts.min.js"></script>
    {if isset($append)}
        {$append nofilter}
    {/if}
</html>