{extends '@layout.tpl'}
{block sidebar}
	<form method="get">
		<label>Řazení</label><br>
		<input type="radio" name="order" value="asc" {if $listingParams.order == "ASC"}checked{/if}> A-Z<br>
		<input type="radio" name="order" value="desc" {if $listingParams.order == "DESC"}checked{/if}> Z-A<br>
		<label>Řazení podle</label><br>
		<input type="radio" name="orderby" value="name" {if $listingParams.orderby == "name"}checked{/if}> Název<br>		
		<input type="radio" name="orderby" value="date" {if $listingParams.orderby == "date"}checked{/if}> Datum<br>
		<div class="Collapse" data-component="collapse">
			<span class="Collapse-control"><label>Kategorie</label> zobrazit/skrýt</span><br>
			<div class="Collapse-item">
			{for $k=1 to count($categories)}
				<u>{$categories.$k.name}</u> <br>
				{foreach $categories.$k as $key => $value}
				{if is_numeric($key)}
				<input type="checkbox" name="cat[{$k}][{$key}]" value="{$key}" {if is($listingParams.categories[{$k}][$key], $key)}checked {/if}>{$value}<br>
				{/if}
				{/foreach}
			{/for}
			</div>
		</div>

		<br>
		<label>Pager</label><br>
		<input type="number" name="page" value="{$listingParams.page}"> z {($listingParams.count/$listingParams.n)|ceil}
		<input type="number" name="n" value="{$listingParams.n}"> na stránku <br>
		<input type="submit" value="Odeslat">
	</form>
{/block}
{block main}
<div class="Listing">
	{foreach $listing as $item}
		{$dataAttr = ["cocktail-id" => $item.id]}
		<a href="/detail/{$item.id}" class="u-inlineBlock">
			{include "../components/productCard.tpl" item=$item class="Listing-item"}
		</a>
	{/foreach}
</div>
{/block}
