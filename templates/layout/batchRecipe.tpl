<div class="Recipe Recipe--batch" data-component="recipe-batch">
	<table>
		<tr>
			<th>Cena porce</th>
			<th>Objem porce</th>
			<th>Počet porcí</th>
			<th>Přísada</th>
			<th>Množství na 1l</th>
			<th>Množství na porci</th>
			<th>1 MU</th>
			<th>MU</th>
			<th>Množství MU</th>
			<th>Cena MU</th>
			<th>Cena na porci</th>
			<th>Cena Σ</th>
			<th>Obchod</th>
			<th>Množství MU</th>
			<th>Množství</th>
			<th>MU</th>
		</tr>
	{foreach $recipe as $rowNr => $row}
		<tr class="Recipe-ingerdient" id="Ingredient-{$rowNr}">
			<td class="js-price-one">CENA PORCE</td>
			<td><input class="js-volume" type="number" name="volume" value="{$volume}"></td>
			<td><input class="js-quantity" type="number" name="quantity" value="50"></td>
			<td class="js-name">{$row.name}</td>
			<td class="js-amount-per-l">{($row.amount / $volume)|number_format:4}</td>
			<td class="js-amount">{$row.amount}</td>
			<td class="js-package">{$row.package}</td>
			<td>{$row.mu}</td>
			<td class="js-mu-quantity">Množství MU</td>
			<td class="js-price-mu">{$row.price}</td>
			<td class="js-price-per-one">CENA NA PORCI</td>
			<td class="js-price-sum">CENA CELKEM</td>
			<td><a href="{$row.link}">{$row.shop}</a></td>
			<td class="js-mu-quantity-raw">Množství MU</td>
			<td class="js-amount-sum">Množství</td>
			<td>{$row.mu}</td>
		</tr>
	{/foreach}
	</table>
</div>