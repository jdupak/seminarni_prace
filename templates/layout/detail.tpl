{extends '@layout.tpl'}
{block sidebar}
{/block}
{block menuLocal}
{if $user.stat}
	<a href={"edit/"|cat:$id|url} class="Button btn-success">Upravit</a>
{/if}
{/block}
{block main}

{if isset($delete)}
	{include file="../components/modal.tpl" title="Smazat koktejl" text="Opravdu chcete smazat tento koktejl?" buttonTrueText="SMAZAT" buttonTrueLink="/detail/{$id}?a=delete"}
{/if}

<div class="Cocktail">
	<div class="Cocktail-image" style="background-image: url({$img});">	
		&nbsp;
	</div>
	<div class="Cocktail-content">
		<h1>{$name}
		{if isset($altNames.1)}
			<small>
				(
				{foreach $altNames as $altName}
					{$altName}  
				{/foreach}
				)
			</small>
		{/if}
		</h1>
		<div class="Cocktail-item h4">
			{$volume}l | {$glass}
		</div>
		<div class="Cocktail-item">
			{$description}
		</div>
		<div class="Cocktail-item">
			<h4>Postup</h4>
			<p>{$procedure}</p>
		</div>
		<div class="Cocktail-item">
			<h4>Servis</h4>
			{$serving}
		</div>

		<h4>Kategorie</h4>
		{foreach $category as $group => $categories}
			{$group}:
			{strip}
			{foreach $categories as $key => $category}
				{if $key > 0}, {/if}{$category}
			{/foreach}
			{/strip}<br>
		{/foreach}

		<h4>Receptura</h4>
		<ul>
			{foreach $recipe as $value}
				<li>{$value.name}: {$value.amount}{$value.mu}
			{/foreach}
		</ul>

		<div class="Popup" data-component="popup">
			<a class="Popup-control" href="#batchRecipe">Dávkový recept</a>
			<div id="batchRecipe" class="Popup-content Popup-content--white Cocktail-batchRecipe">
		        {include "./batchRecipe.tpl"}
            </div>
        </div>

        <div class="Cocktail-item">
            Vytvořeno: {$date}      
        </div>
	</div>
</div>
{/block}