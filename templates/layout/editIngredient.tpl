{extends '@layout.tpl'}
{block sidebar}

{/block}
{block main}
<div class="Container Container--big">
	<form action="" method="post" class="Form" enctype="multipart/form-data">
			{strip}
		<div class="Tabs-item" id="basic">
			<div class="Form-row">
				{include "../components/input.tpl" label="Název" name="f_ingredient[name]" required=true value=$name labelClass="u-alignBottom" inputClass="Form-input--bottomLine h1"}
			</div>
			<div class="Form-row">
				{* {include "../components/input.tpl" label="ID2" name="f_ingredient[id2]" type="number" value=$id2 inputClass="Form-input--noborder"} *}
				<div class="Form-cell Form-cell--label{if isset($labelClass)} {$labelClass}{/if}">
					<label for="f_id2">Alternativa k</label>
				</div>
				<div class="Form-cell Form-cell--input">
					<input type="text" name="f_ingredient[id2]" class="Form-input Form-input--noborder" data-component="ingredient-variants">
				</div>
			</div>

			<div class="Form-row">
				{include "../components/input.tpl" label="Množství" name="f_ingredient[package]" required=true type="number" value=$package inputClass="Form-input--noborder"}
			</div>
			<div class="Form-row">
				{include "../components/input.tpl" label="MU" name="f_ingredient[mu]" required=true value=$mu labelClass="u-alignBottom" inputClass="Form-input--noborder"}
			</div>
			<div class="Form-row">
				{include "../components/input.tpl" label="Cena" name="f_ingredient[price]" type="number" value=$price labelClass="u-alignBottom" inputClass="Form-input--noborder"}
			</div>
			<div class="Form-row">
				{include "../components/input.tpl" label="Obchod" name="f_ingredient[shop]" value=$shop labelClass="u-alignBottom" inputClass="Form-input--noborder"}
			</div>
			<div class="Form-row">
				{include "../components/input.tpl" label="odkaz" name="f_ingredient[link]" value=$link labelClass="u-alignBottom" inputClass="Form-input--noborder"}
			</div>
			{/strip}
		</div>
		<div class="Form-row">
			<div class="Form-cell">
				<a href="{'del'|url_r}" class="Button Button--warning">Smazat</a>
			</div>
			<div class="Form-cell">
				<input type="submit" class="Button" value="Odeslat">
			</div>

		</div>
	</form>
</div>
{/block}