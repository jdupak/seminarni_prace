{extends '@layout.tpl'}
{block sidebar}
{/block}
{block main}
<div class="Container Container--big">
	<div class="">
		<form method="post" action="" id="login">
			<div style="margin-bottom: 10px;">
				{include "../components/input.tpl" label="Username" name="login[username]" required=true type="text" value="" inputClass="Form-input"}
				{include "../components/input.tpl" label="Password" name="login[password]" required=true type="password" value="" inputClass="Form-input"}
				
			</div>
			<input type="submit" name="submit" class="Button" value="Odeslat">
		</form>
	</div>																				
</div>
{/block}