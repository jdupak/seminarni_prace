{extends '@layout.tpl'}
{block sidebar}

{/block}
{block main}
<div class="Container Container--big">
	<form action="" method="post" class="Form" enctype="multipart/form-data">
		<div data-component="tabs" data-setting-first-on="true">
			<div class="button-Group">
				<a class="Tabs-control Button" href="#basic" data-target="basic">Basic</a>
				<a class="Tabs-control Button" href="#recipe" data-target="recipe">Recept</a>
				<a class="Tabs-control Button" href="#categories" data-target="categories">Kategorie</a>
			</div>
			{strip}
			<div class="Tabs-item" id="basic">
				<div class="Form-row">
					{include "../components/input.tpl" label="Název" name="f_cocktail[name]" value=$name labelClass="u-alignBottom" inputClass="Form-input--bottomLine h1"}
				</div>
				<div class="Form-row">
					{include "../components/input.tpl" label="Alternativní názvy<br><small>(odd. středníkem)</small>" name="f_cocktail[altNames]" value=$altNames inputClass="Form-input--noborder h2"}
				</div>
				<div class="Form-row">
					{include "../components/input.tpl" label="Standardní objem [l]" name="f_cocktail[volume]" value=$volume inputClass="Form-input--noborder"}
				</div>
				<div class="Form-row">
					{include "../components/input.tpl" label="Obrázek" type="text" name="f_cocktail[img]" value=$img inputClass="Form-input--noborder" other="data-component=elfinder"}
				</div>
				<div class="Form-row">
					{include "../components/input.tpl" label="Servis" textarea=true name="f_cocktail[serving]" value=$serving inputClass="Form-input--noborder"}
				</div>
				<div class="Form-row">
					{include "../components/input.tpl" label="Příprava" textarea=true name="f_cocktail[procedure]" value=$procedure inputClass="Form-input--noborder"}
				</div>
				<div class="Form-row">
					{include "../components/input.tpl" label="Popis" textarea=true name="f_cocktail[description]" value=$description inputClass="Form-input--noborder"}
				</div>
			</div>
			<div class="Tabs-item" id="recipe" style="display: none;">
				<table data-component="recipe-table">
					<tr>
						<th>
							Surovina
						</th>
						<th>
							&nbsp;
						</th>
						<th>
							Množství
						</th>
						<th>
							Balení
						</th>
						<th>
							MU
						</th>
						<th>
							Cena
						</th>
						<th>
							Obchod
						</th>
						<th>
							Odkaz
						</th>
						<th>
							&nbsp;
						</th>
					</tr>
				{foreach $recipe as $n => $row}
					<tr data-row-nr="{$n}">
						<td>
							<input type="text" class="f_name" name="f_recipe_[{$n}]" value="{$row['name']}" data-component="ingredients">
							<input type="hidden" class="f_id" name="f_recipe[{$n}][id]" value="{$row['id']}">
						</td>
						<td>
							<input type="number" class="f_a" name="f_recipe[{$n}][amount]" step="0.001" value="{$row['amount']}">
						</td>
						<td class="f_package">
							{$row['package']}
						</td>
						<td class="f_mu">
							{$row['mu']}
						</td>
						<td class="f_price">
							{$row['price']}
						</td>
						<td class="f_shop">
							{$row['shop']}
						</td>
						<td>
							<a href="{$row['link']}" class="f_link">{$row['link']}</a>
						</td>
						<td>
							<span class="Button js-create-ingredient">*</span>
						</td>
					</tr>
				{/foreach}
				</table>
			</div>
			<div class="Tabs-item" id="categories" style="display: none;">
				{for $k=1 to count($categories)}
					<label>{$categories.$k.name}</label> <br>
					{foreach $categories.$k as $key => $value}
					{if is_numeric($key)}
					<input type="checkbox" name="cat[]" value="{$key}" {if $key|in_array:$cocktailCat}checked{/if}>{$value}<br>
					{/if}
					{/foreach}
				{/for}
			</div>
			{/strip}
		</div>
		<div class="Form-row">
			<div class="Form-cell">
				<a href="{'del'|url_r}" class="Button Button--warning">Smazat</a>
			</div>
			<div class="Form-cell">
				<input type="submit" class="Button" value="Odeslat">
			</div>

		</div>
	</form>
	<div id="elfinder"></div>
</div>
{/block}
{block js}
	<script type="text/javascript">
		$('f_cocktail[img]').on('dblclick', function(event) {
			var elfinder = window.open("/elfinder/index.html", "File Manager", "width=800,height=400");
			var file = null;
			elfinder.onbeforeunload = function(){
				file = elfinder.selectfile;
				$('f_cocktail[img]').val(file);
			}
		});
		</script>
{/block}