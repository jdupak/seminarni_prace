{include "base/header.tpl"}
{if isset($errorMessage)}
	<div class="Message-wrapper">
		{foreach $errorMessage as $item}
			<div class="alert alert-{$item.type} alert-dismissible show" role="alert">
				{$item.msg nofilter}
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>
		{/foreach}	
	</div>
{/if}
<div class="Content">
	<div class="Wrapper" style="background-color: #f4a836">
		<header class="Header Container" style="height: 25%;background-color: #f4a836;">
			{block header}
			{/block}
		</header>

		<main class="Main Container" style="">
			<div class="Container" style="max-width: 900px;">
				<h1 style="font-size: 120px; color: white; text-align: center; line-height: 70%;"><small>The</small><br>MixingDB</h1>
			</div>
			<div class="Container Grid" style="margin: 12% auto; text-align: center; padding: 0;">
				<div class="Grid-cell u-size1of3">
					<a href={"listing?cat[][]=1"|url} class="h3 u-block">
						{$categories.1.1}
					</a>
					<a href={"listing?cat[][]=2"|url} class="h3 u-block">
						{$categories.1.2}
					</a>
					<a href={"listing?cat[][]=3"|url} class="h3 u-block">
						{$categories.2.3}
					</a>
				</div>
				<div class="Grid-cell u-size1of3">
					<a href={"listing?cat[][]=4"|url} class="h3 u-block">
						{$categories.2.4}
					</a>
					<a href={"listing?cat[][]=5"|url} class="h3 u-block">
						{$categories.2.5}
					</a>
					<a href={"listing?cat[][]=6"|url} class="h3 u-block">
						{$categories.2.6}
					</a>
				</div>
				<div class="Grid-cell u-size1of3">
					<a href={"listing?cat[][]=1"|url} class="h3 u-block">
						{$categories.2.7}
					</a>
					<a href={"listing?cat[][]=2"|url} class="h3 u-block">
						{$categories.4.19}
					</a>
					<a href={"listing"|url} class="h3 u-block">
						...
					</a>
				</div>
				
			</div>
		</main>
		
		<footer class="Footer">
			©{'Y'|date}, Jakub Dupák		
		</footer>
	</div>
</div>
{capture js}
	{block js}
	{/block}
{/capture}

{include "base/footer.tpl" append=$smarty.capture.js}

{if isset($debug)}
	{debug}
{/if}