{extends '@layout.tpl'}
{block sidebar}

{/block}
{block main}
<div class="Container Container--big">
	<table>
		<tr>
			<th>Název</th>
			<th>Balení</th>
			<th>Cena</th>
			<th>Obchod</th>
			<th>Odkaz</th>
			<th>&nbsp;</th>
			<th>&nbsp;</th>
		</tr>
	{foreach $list as $k => $ingredient}
		<tr>
			<td>{$ingredient.name}</td>
			<td>{$ingredient.package}{$ingredient.mu}</td>
			<td>{$ingredient.price}</td>
			<td>{$ingredient.shop}</td>
			<td><a href="{$ingredient.link}">{$ingredient.link}</a></td>
			<td><a href="{"ingredient/"|cat:$ingredient.id|cat:"/edit/"|url}" class="Button">EDIT</a></td>
			<td><a href="{"ingredient/"|cat:$ingredient.id|cat:"/del/"|url}" class="Button Button--warning">DELETE</a></td>
		</tr>
	{/foreach}
	</table>
</div>
{/block}