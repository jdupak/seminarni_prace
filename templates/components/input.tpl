<div class="Form-cell Form-cell--label{if isset($labelClass)} {$labelClass}{/if}">
	<label for="f_{$name}">{$label}</label>
</div>
{strip}
<div class="Form-cell Form-cell--input">
	<{if isset($textarea)}textarea
		{else}input type="{if isset($type)}{$type}{else}text{/if}"
			value="{$value}"
	{/if} name="{$name}"
		id="f_{$name}"
		{if isset($required)}required{/if}
		placeholder="{if isset($placeholder)}{$placeholder}{else}Vložte text{/if}"
		class="Form-input{if isset($inputClass)} {$inputClass}"{/if}
		{if isset($other)} {$other}{/if}
		>
	{if isset($textAfter)} {$textAfter}{/if}
		{if isset($textarea)}{$value}</textarea>
	{/if}
</div>
{/strip}