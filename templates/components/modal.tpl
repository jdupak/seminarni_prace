/**
 * Bootstrap modal component
 * @param {text} title
 * @param {HTML|text} text
 * @param {URL} $buttonTrueLink
 * @param {text} $buttonTrueText
 * @param {text} $buttonFalseText
 */
<div {* class="modal" *} tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{$title}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>{$text|nofilter}</p>
      </div>
      <div class="modal-footer">
        <a type="button" {if isset($buttonTrueLink)}src="{$buttonTrueLink}"{/if} class="btn btn-primary">{$buttonTrueText}</a>
        <a type="button" class="btn btn-secondary" data-dismiss="modal">{$buttonFalseText}</a>
      </div>
    </div>
  </div>
</div>