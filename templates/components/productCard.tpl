<div class="ProductCard{if isset($class)} {$class}{/if}"{if isset($dataAttr)}{foreach $dataAttr as $key => $value} data-{$key}="{$value}"{/foreach}{/if}>
	<div class="ProductCard-image" style="background-image: url('{$item.img}')">
		&nbsp;
	</div>
	<div class="ProductCard-body">
		<h3 class="ProductCard-title">{$item.name}</h3>
	</div>
</div>