{include "base/header.tpl"}
{if isset($errorMessage)}
	<div class="Message-wrapper">
		{foreach $errorMessage as $item}
			<div class="alert alert-{$item.type} alert-dismissible show" role="alert">
				{$item.msg nofilter}
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>
		{/foreach}	
	</div>
{/if}
<div class="Content">
	<aside class="Sidebar">
		<a href={""|url} style="font-size: 36px; color: white; text-decoration: none;">
			<h1><small>The</small><br>MixingDB</h1>
		</a>
		{block sidebar}
		{/block}
	</aside>

	<div class="Wrapper">
		<header class="Header">
			<div style="position: absolute; top: 0; right: 5px;">
				{block menuLocal}{/block}
				{if $user.stat}
					<a href={"ingredient/"|url} class="Button btn-success">Správa ingrediencí</a>
				{/if}
				&nbsp;
				<a href={""|url} class="Button">HP</a>
				<a href={"listing"|url} class="Button">Výpis</a>
			{if !$user.stat}
				<a href={"register"|url} class="Button">Register</a>
				<a href={"login"|url} class="Button">Login</a>
			{else}
				<a href={"logout"|url} class="Button">Logout ({$user.username})</a>
			{/if}
			</div>
			{block header}
			{/block}
		</header>

		<main class="Main">
			{block main}
			{/block}
		</main>
		
		<footer class="Footer">
			{block footer}
			{/block}		
		</footer>
	</div>
</div>
{capture js}
	{block js}
	{/block}
{/capture}

{include "base/footer.tpl" append=$smarty.capture.js}

{if isset($debug)}
	{debug}
{/if}